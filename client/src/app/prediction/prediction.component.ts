import {
    AfterViewInit,
    Component,
    ElementRef,
    OnInit,
    ViewChild
} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {TEAM_LIST, VENUE_LIST} from '../core/core.constants';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-prediction',
    templateUrl: './prediction.component.html',
    styleUrls: ['./prediction.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
            state('expanded', style({height: '*'})),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
    ]
})

export class PredictionComponent implements OnInit, AfterViewInit {

    themeColor: string;
    teamList = TEAM_LIST;
    predictionForm: FormGroup;
    venueList = VENUE_LIST;
    firstFormGroup: FormGroup;
    secondFormGroup: FormGroup;
    thirdFormGroup: FormGroup;
    fourthFormGroup: FormGroup;
    BatFirstList = [];

    constructor(private formBuilder: FormBuilder) {
    }

    ngOnInit(): void {
        this.predictionForm = this.formBuilder.group({
            team1: new FormControl('', Validators.required),
            team2: new FormControl('', Validators.required),
            venue: new FormControl('', Validators.required),
        });
        this.firstFormGroup = this.formBuilder.group({
            team1: ['', Validators.required]
        });
        this.secondFormGroup = this.formBuilder.group({
            team2: ['', Validators.required]
        });
        this.thirdFormGroup = this.formBuilder.group({
            venue: ['', Validators.required]
        });
        this.fourthFormGroup = this.formBuilder.group({
            firstBat: ['', Validators.required]
        });
    }

    ngAfterViewInit(): void {
        setInterval(() => {
            this.themeColor = sessionStorage.getItem('theme');
        }, 1000);
    }

    getMatchData() {
        this.BatFirstList = [this.firstFormGroup.get('team1').value, this.secondFormGroup.get('team2').value];
    }


    getPredictionData(event) {
        const predictionObj = {
            team1: this.firstFormGroup.get('team1').value,
            team2: this.secondFormGroup.get('team2').value,
            venue: this.thirdFormGroup.get('venue').value,
            firstBat: this.fourthFormGroup.get('firstBat').value
        };
        console.log(predictionObj);
    }
}
