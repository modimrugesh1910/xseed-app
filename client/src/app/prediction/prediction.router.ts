import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PredictionComponent} from './prediction.component';

const widgetRoutes: Routes = [
  {path: '', component: PredictionComponent, data: {animation: 'prediction'}},
];

@NgModule({
  imports: [
    RouterModule.forChild(widgetRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class PredictionRouter {
}
