import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MAT_DATE_LOCALE, MatNativeDateModule} from '@angular/material';

import {MaterialModule} from '../shared/angular-material.module';
import {CoreModule} from '../core/core.module';
import {PredictionComponent} from './prediction.component';
import { PredictionRouter } from './prediction.router';

@NgModule({
    imports: [
        CommonModule,
        FlexLayoutModule,
        MaterialModule,
        ReactiveFormsModule,
        FormsModule,
        CoreModule,
        PredictionRouter
    ],
    declarations: [
        PredictionComponent
    ],

    exports: [],
    providers: [
        {provide: MAT_DATE_LOCALE, useValue: 'en-GB'}
    ]
})
export class PredictionModule {
}


