import {
    AfterViewInit,
    Component,
    ElementRef,
    OnInit,
    ViewChild
} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
// import {exportTOExcel, getDiffColor} from '../../core/core.functions';
import {TableService} from '../tables.service';
import {fromEvent as observableFromEvent, BehaviorSubject} from 'rxjs';
import {distinctUntilChanged, debounceTime, switchMap} from 'rxjs/operators';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CommonService} from '../../core/common.service';
import {UserService} from '../../core/services/user.service';
import {TEAM_LIST} from "../../core/core.constants";

@Component({
    selector: 'app-watchlist-table',
    templateUrl: './watchlist-table.component.html',
    styleUrls: ['./watchlist-table.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
            state('expanded', style({height: '*'})),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
    ]
})

export class WatchlistTableComponent implements OnInit, AfterViewInit {
    dataSource: any;
    fundTableForm: FormGroup;
    headerNames: Array<string> = ['season', 'date', 'city'];
    columnsToDisplay: any;
    fundDataSource: any;
    selectedData: any;
    showList = false;
    showTable = true;

    themeColor: string;

    teamList = TEAM_LIST;

    @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: false}) sort: MatSort;
    @ViewChild('filter', {static: false}) filter: ElementRef;

    constructor(private readonly tableService: TableService, private formBuilder: FormBuilder,
                private userService: UserService) {
    }

    ngOnInit(): void {
        this.fundTableForm = this.formBuilder.group({
            fundSchemeName: new FormControl('', Validators.required)
        });
    }

    ngAfterViewInit(): void {
        this.tableService.fetchData().subscribe((res) => {
            res.data.map(function (item) {
                delete item._id;
                return item;
            });
            this.dataSource = new MatTableDataSource(res.data);
            this.columnsToDisplay = res.data;
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;

            observableFromEvent(this.filter.nativeElement, 'keyup').pipe(
                debounceTime(150),
                distinctUntilChanged(), )
                .subscribe(() => {
                    if (!this.dataSource) {
                        return;
                    }
                    this.dataSource.filter = this.filter.nativeElement.value;
                });
        });
    }

    selectTheme(event) {
        if (event.value && event.value !== '') {
            this.themeColor = event.value;
            sessionStorage.setItem('theme', this.themeColor);
            this.userService.subjectThemeColor.next(event.value);
        }
    }

    private getFundRawData({value, valid}: { value: any, valid: boolean }) {
        return this.fundTableForm.getRawValue();
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    getSelectedData(data) {
        this.showList = true;
        this.showTable = false;
        this.selectedData = data;
    }

    getBackToTable() {
        this.showList = false;
        this.showTable = true;
    }

    downloadXLSX(data) {
        // exportTOExcel('Exchange', data, []);
    }
}

