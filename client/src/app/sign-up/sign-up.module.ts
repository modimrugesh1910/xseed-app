import {NgModule} from '@angular/core';
import {
    MatCardModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatInputModule,
    MatToolbarModule
} from '@angular/material';
import {CommonModule} from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout';
import {Routes, RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CoreModule} from '../core/core.module';
import {SignUpComponent} from './sign-up.component';

const routes: Routes = [
    {path: '', component: SignUpComponent},
];

@NgModule({
    imports: [
        MatCardModule,
        CommonModule,
        CoreModule,
        FlexLayoutModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatInputModule,
        MatToolbarModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        SignUpComponent
    ],
    exports: [
        RouterModule
    ],
    providers: []
})
export class SignUpModule {
}
