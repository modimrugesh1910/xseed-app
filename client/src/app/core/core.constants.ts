export const TEAM_LIST = [
    {
        'teamName': 'Chennai Super Kings',
        'teamClass': 'CSK'
    },
    {
        'teamName': 'Delhi Capitals',
        'teamClass': 'DC'
    },
    {
        'teamName': 'Kings XI Punjab',
        'teamClass': 'KP'
    },
    {
        'teamName': 'Kolkata Knight Riders',
        'teamClass': 'KKR'
    },
    {
        'teamName': 'Mumbai Indians',
        'teamClass': 'MI'
    },
    {
        'teamName': 'Rajasthan Royals',
        'teamClass': 'RR'
    },
    {
        'teamName': 'Royal Challengers Bangalore',
        'teamClass': 'RCB'
    },
    {
        'teamName': 'Sunrisers Hyderabad',
        'teamClass': 'SRH'
    },
];

export const VENUE_LIST = [
    'Abu Dhabi',
    'Ahmedabad',
    'Bangalore',
    'Bloemfontein',
    'Cape Town',
    'Centurion',
    'Chennai',
    'Chandigarh',
    'Cuttack',
    'Delhi',
    'Dharamsala',
    'Durban',
    'East London',
    'Hyderabad',
    'Indore',
    'Jaipur',
    'Johannesburg',
    'Kanpur',
    'Kimberley',
    'Kochi',
    'Kolkata',
    'Mumbai',
    'Pune',
    'Raipur',
    'Ranchi',
    'Rajkot',
    'Sharjah',
    'Port Elizabeth',
    'Visakhapatnam'
];
