export const menus = [
    {
        'name': 'Table',
        'icon': 'list',
        'link': '/auth/table',
        'open': false,
    }, {
        'name': 'Prediction',
        'icon': 'search',
        'link': '/auth/prediction',
        'open': false,
    }
];

