import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormGroup, FormBuilder, Validators, NgForm} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {URL_API} from '../core/core.apis';
import {UserService} from '../core/services/user.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    // loginForm: FormGroup;
    // formErrors = {
    //     'email': '',
    //     'password': ''
    // };
    // validationMessages = {
    //     'email': {
    //         'required': 'Please enter your email',
    //         'email': 'please enter your vaild email'
    //     },
    //     'password': {
    //         'required': 'please enter your password',
    //         'pattern': 'The password must contain numbers and letters',
    //         'minlength': 'Please enter more than 4 characters',
    //         'maxlength': 'Please enter less than 25 characters',
    //     }
    // };

    model = {
        email : '',
        password: ''
    };
    emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    serverErrorMessages: string;

    constructor(private router: Router, private fb: FormBuilder,
                private userService: UserService, private httpClient: HttpClient) {
    }

    ngOnInit() {
        if (this.userService.isLoggedIn()) {
            this.router.navigateByUrl('/auth/table');
        }
    }

    // buildForm() {
    //     this.loginForm = this.fb.group({
    //         'email': ['', [
    //             Validators.required,
    //             Validators.email
    //         ]
    //         ],
    //         'password': ['', [
    //             Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
    //             Validators.minLength(6),
    //             Validators.maxLength(25)
    //         ]
    //         ],
    //     });
    //
    //     this.loginForm.valueChanges.subscribe(data => this.onValueChanged(data));
    //     this.onValueChanged();
    // }
    //
    // onValueChanged(data?: any) {
    //     if (!this.loginForm) {
    //         return;
    //     }
    //     const form = this.loginForm;
    //     for (const field in this.formErrors) {
    //         if (Object.prototype.hasOwnProperty.call(this.formErrors, field)) {
    //             this.formErrors[field] = '';
    //             const control = form.get(field);
    //             if (control && control.dirty && !control.valid) {
    //                 const messages = this.validationMessages[field];
    //                 for (const key in control.errors) {
    //                     if (Object.prototype.hasOwnProperty.call(control.errors, key)) {
    //                         this.formErrors[field] += messages[key] + ' ';
    //                     }
    //                 }
    //             }
    //         }
    //     }
    // }
    //
    // login() {
    //     localStorage.setItem(URL_API.LOCAL_STORAGE.KEYS.TOKEN, 'eyJhbGciOiJIUzI1NiIsInR565dsrcCI6IkpXVCJ9.eyJ1c2VySWQiOjEwMjgsIm9yZ0lkIjoiT1pfT1JHX09JWk9NIiwidXlsIjoiYnJvQG9pem9tLmNvbSIsImlhdCI6MTU2NDMwOTgyMSwiZXhwIjoxNTY0OTE0NjIxLCJpc3MiOiJ1WGd6aHZCRFRlRmdTWDhyb1lFYlhwQUltNVpOWnNmVCJ9.0Y9sDmRQnVwhFsSqcgy-z9h0zDttgA3Oh4CS91');
    //     this.router.navigate(['/auth/table/fixed']);
    // }


    onSubmit(form : NgForm){
        this.userService.login(form.value).subscribe(
            res => {
                this.userService.setToken(res['token']);
                this.router.navigateByUrl('/userprofile');
            },
            err => {
                this.serverErrorMessages = err.error.message;
            }
        );
    }
}

