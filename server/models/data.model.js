const mongoose = require('mongoose');
const _ = require('lodash');

var dataSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: 'id can\'t be empty',
        unique: true
    },
    season: {
        type: Number,
        required: 'season can\'t be empty'
    },
    city: {
        type: String,
        required: 'city can\'t be empty'
    },
    team1: {
        type: String,
        required: 'team1 can\'t be empty'
    },
    team2: {
        type: String,
        required: 'team2 can\'t be empty'
    },
    toss_winner: {
        type: String,
        required: 'toss_winner can\'t be empty'
    },
    toss_decision: {
        type: String,
        required: 'toss_decision can\'t be empty'
    },result: {
        type: String,
        required: 'result can\'t be empty'
    },dl_applied: {
        type: String,
        required: 'dl_applied can\'t be empty'
    },winner: {
        type: String,
        required: 'winner can\'t be empty'
    },win_by_runs: {
        type: Number,
        required: 'win_by_runs can\'t be empty'
    },win_by_wickets: {
        type: Number,
        required: 'win_by_wickets can\'t be empty'
    },player_of_match: {
        type: String,
        required: 'player_of_match can\'t be empty'
    },venue: {
        type: String,
        required: 'venue can\'t be empty'
    },umpire1: {
        type: String,
        required: 'umpire1 can\'t be empty'
    },umpire2: {
        type: String,
        required: 'umpire2 can\'t be empty'
    },umpire3: {
        type: String,
        required: 'umpire3 can\'t be empty'
    }
});

mongoose.model('Data', dataSchema);