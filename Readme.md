**XSEED FUll Stack API**
----

### Github Link - https://bitbucket.org/modimrugesh1910/xseed-app/src

* To run apis Application
 
1. npm install
2. nodemon app.js or node app.js
 
** Root URL will be http://localhost:3000/api

* Import data into local system 

If you have MongoDB compass or robomongo you can directly import in database collection using import option

or using ubuntu/apple terminal - 

mongoimport -d XSEED_LOCAL -c data --type csv --file matches2d4c40a.csv --headerline

here matches2d4c40a.csv is downloaded given csv file on hackerearth.
 
 * To run Front-end Application 
 
 ## Development Build**
 
 1. `cd client`
 2. `npm install`
 3. `npm start`
 
 It works on http://localhost:4200/register
 
 please first register user then use login functionality it will work better.
 
 Please make sure you have installed latest angular-cli on your local machine.
 
 To install angular-cli please refer - https://angular.io/cli
 
 ## Production Build
 
 1. `npm run build`


## Work Done - 

* Upload the CSV file to the database.
* When a user clicks on a listed item (cricket match), comprehensive info about the match must be displayed on a separate dedicated page.
* Writing tests 
* Implement sign-up, log in and log out functionalities. You have to create the user-auth schema in the database.
* Implement a functionality in the front-end​ to search for a team’s matches using the team name.
* Implement paging ​to display the results properly on the front-end​.
* Provide a filter based on the various years (seasons) of the tournament on the front-end​. Choosing a year(s) should show only the matches from those years.
* Choose Favourite team, change color theme of web app according color of team
* started working on predict the results of a future match, built full UI, backend working is going on...
* Zip all your source code, executables, screenshots and upload the folder.

* If you have any query shoot mail on - modimrugesh1910@gmail.com